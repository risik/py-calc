import pyparsing
import pytest

from vesel.parser import (
    EvalGrammar,
    EvalParsingError,
)

parse_test_data = [
    # math
    ("5", "float(5.0)"),
    ("5+3+2", "add(add(float(5.0),float(3.0)),float(2.0))"),
    ("5-3-2", "sub(sub(float(5.0),float(3.0)),float(2.0))"),
    ("2*3/2", "div(mult(float(2.0),float(3.0)),float(2.0))"),
    ("2+2*2", "add(float(2.0),mult(float(2.0),float(2.0)))"),
    ("(2+3)*5", "mult(add(float(2.0),float(3.0)),float(5.0))"),
    ("2*(3+5)", "mult(float(2.0),add(float(3.0),float(5.0)))"),
    ("-5", "uminus(float(5.0))"),
    ("+5", "float(5.0)"),
    ("---5", "uminus(float(5.0))"),
    ("-+-5", "float(5.0)"),
    #
    # bool
    ("True+true", "add(bool(True),bool(True))"),
    ("Yes-yes", "sub(bool(True),bool(True))"),
    ("False*false", "mult(bool(False),bool(False))"),
    ("No/no", "div(bool(False),bool(False))"),
    #
    # str
    ('"Hello"', 'str("Hello")'),
    ('"World!"', 'str("World!")'),
    ('"N\'avy"', 'str("N\'avy")'),
    ("'N\"avy'", 'str("N\\"avy")'),
    ("\"Hello\" + 'World!'", 'add(str("Hello"),str("World!"))'),
    ("'Hello\\ \"dry\"'", 'str("Hello\\\\ \\"dry\\"")'),
]

parse_neg_test_data = [
    (None, EvalParsingError),
    ("", pyparsing.ParseException),
    ("5 6", pyparsing.ParseException),
    ("5+", pyparsing.ParseException),
    ("*5", pyparsing.ParseException),
    ("a/", pyparsing.ParseException),
    ("5a", pyparsing.ParseException),
]


class TestCalcGrammar:
    def setup_class(self):
        self.fgm = EvalGrammar()

    @pytest.mark.parametrize("src,expected", parse_test_data)
    def test_parser(self, src: str, expected: str):
        parse_tree = self.fgm.parse(src)

        actual = str(parse_tree.root)

        assert actual == expected

    @pytest.mark.parametrize("src,expected", parse_neg_test_data)
    def test_parser_neg(self, src: str, expected: Exception):
        with pytest.raises(expected):
            self.fgm.parse(src)
