"""
This module contains AST (abstract syntax tree) for 'Vesel'.
"""

from abc import ABC
from enum import Enum


class ExpType(Enum):
    """
    Types supported by 'Vesel':
    * Unk: still unknown
    * Bool: boolean expression
    * Num: numeric (float) expression
    * Str: string expression
    """

    Unk = 0
    Bool = 1
    Num = 2
    Str = 4


class ExpressionNode:
    """Base expression node"""


class ExpressionConst(ExpressionNode, ABC):
    """Base constant node"""

    def __init__(self, val_type: ExpType) -> None:
        super().__init__()
        self.val_type = val_type


class NumConst(ExpressionConst):
    """Numeric constant node"""

    def __init__(self, value: float) -> None:
        super().__init__(ExpType.Num)
        self.value = value

    def __repr__(self) -> str:
        return "float({})".format(self.value)


class BoolConst(ExpressionConst):
    """Boolean constant node"""

    def __init__(self, val: bool) -> None:
        super().__init__(ExpType.Bool)
        self.val = val

    def __repr__(self) -> str:
        return "bool({})".format(self.val)


class StrConst(ExpressionConst):
    """String constant node"""

    def __init__(self, val: str) -> None:
        super().__init__(ExpType.Str)
        self.val = val

    def __repr__(self) -> str:
        res = (
            self.val.encode("unicode_escape")
            .decode("ASCII")
            .replace('"', '\\"')
        )
        return 'str("{}")'.format(res)


class BinaryOperation(ExpressionNode):
    """Base binary operation node"""

    def __init__(self, left: ExpressionNode, right: ExpressionNode) -> None:
        super().__init__()
        self.left = left
        self.right = right


class UnaryOperation(ExpressionNode):
    """Base unary operation node"""

    def __init__(self, val: ExpressionNode) -> None:
        super().__init__()
        self.val = val


class OperationAdd(BinaryOperation):
    """Arithmetic 'plus' operation node"""

    def __repr__(self) -> str:
        return "add({},{})".format(self.left, self.right)


class OperationSub(BinaryOperation):
    """Arithmetic 'minus' operation node"""

    def __repr__(self) -> str:
        return "sub({},{})".format(self.left, self.right)


class OperationMult(BinaryOperation):
    """Arithmetic 'mult' operation node"""

    def __repr__(self) -> str:
        return "mult({},{})".format(self.left, self.right)


class OperationDiv(BinaryOperation):
    """Arithmetic 'div' operation node"""

    def __repr__(self) -> str:
        return "div({},{})".format(self.left, self.right)


class OperationUnaryMinus(UnaryOperation):
    """Unary 'minus' operation node"""

    def __repr__(self) -> str:
        return "uminus({})".format(self.val)


class EvalAst:
    """
    Abstract syntax tree

    :param root: root node
    """

    def __init__(self, root: ExpressionNode) -> None:
        super().__init__()
        self.root = root

    def __repr__(self) -> str:
        return "ast({})".format(self.root)
