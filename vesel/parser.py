"""
This module contains parser for 'Vesel' and required exception class.
"""

import pyparsing as pp

from vesel.ast import (
    OperationAdd,
    OperationSub,
    OperationMult,
    OperationDiv,
    NumConst,
    BoolConst,
    StrConst,
    ExpressionNode,
    OperationUnaryMinus,
    EvalAst,
)
from vesel.common_stuff import EvalError


class EvalParsingError(EvalError):
    """Exception that may raise during 'Vesel' expression parsing"""


class EvalGrammar:
    """
    'Vesel' grammar.

    :ivar bnf: 'pyparsing' based grammar. Lazy initialized class-member.
    :vartype bnf: pyparsing.ParserElement
    """

    bnf: pp.ParserElement = None

    def __init__(self) -> None:
        super().__init__()
        if self.bnf is None:
            self.bnf = self.make_bnf()

    def make_bnf(self) -> pp.ParserElement:
        """
        Make BNF for 'Eval' grammar.

        TS - terminal symbols
        NTS - non-terminal symbols

        plus, minus, mult, div: TS for basic math operators.
        addop: NTS for additive level ops (plus and minus)
        multop: NTS for multiple level ops (plus and minus)
        lpar, rpar: TS left and right parenthes
        fnumber: TS float number
        boolval: TS boolean value
        str_const: NTS string const (any of TSs 'str' or "str" terninals)
        const: NTS any constant
        ident: TS identifier (variable name etc)
        atom: NTS atomic part (const, variable or expression in parentheses)
        operators hierarchy (from high to low):
        * mlt_expr: NTS mult/div
        * add_expr: NTS add/sub
        """

        # pylint: disable = too-many-locals

        plus, minus, mult, div = map(pp.Literal, "+-*/")
        lpar, rpar = map(pp.Suppress, "()")
        addop = plus | minus
        multop = mult | div

        fnumber = pp.Regex(r"[+-]?\d+(?:\.\d*)?(?:[eE][+-]?\d+)?")
        fnumber.setParseAction(self.create_num_const)

        # boolean const
        boolval = pp.Regex(r"[Tt]rue|[Ff]alse|[Yy]es|[Nn]o")
        boolval.setParseAction(self.create_bool_const)

        dq_str = pp.Regex(r"[\"](?P<body>[^\"]*)[\"]")
        sq_str = pp.Regex(r"[\'](?P<body>[^\']*)[\']")
        str_const = (dq_str | sq_str).setParseAction(self.create_str_const)

        const = str_const | fnumber | boolval

        add_expr = pp.Forward()

        atom = pp.ZeroOrMore(addop) + (const | (lpar + add_expr + rpar))
        atom.setParseAction(self.create_uminus)

        mlt_expr = atom + pp.ZeroOrMore(multop + atom)
        mlt_expr.setParseAction(self.multiple_level_op)

        add_expr <<= mlt_expr + pp.ZeroOrMore(addop + mlt_expr)
        add_expr.setParseAction(self.additive_level_op)

        return add_expr

    @staticmethod
    def additive_level_op(toks: pp.ParseResults):
        """Create AST part for additive level operators"""
        current_node = toks[0]
        if len(toks) == 1:
            return current_node
        for i in range(0, int((len(toks) - 1) / 2)):
            if toks[i * 2 + 1] == "+":
                current_node = OperationAdd(current_node, toks[i * 2 + 2])
            elif toks[i * 2 + 1] == "-":
                current_node = OperationSub(current_node, toks[i * 2 + 2])
            else:
                raise AssertionError(
                    "expected '+'|'-' but found: {}".format(toks[i * 2 + 1])
                )

        return current_node

    @staticmethod
    def multiple_level_op(toks: pp.ParseResults):
        """Create AST part for multiple level operators"""
        current_node = toks[0]
        if len(toks) == 1:
            return current_node
        for i in range(0, int((len(toks) - 1) / 2)):
            if toks[i * 2 + 1] == "*":
                current_node = OperationMult(current_node, toks[i * 2 + 2])
            elif toks[i * 2 + 1] == "/":
                current_node = OperationDiv(current_node, toks[i * 2 + 2])
            else:
                raise AssertionError(
                    "expected '+'|'-' but found: {}".format(toks[i * 2 + 1])
                )

        return current_node

    @staticmethod
    def create_num_const(toks: pp.ParseResults):
        """Create AST part for numeric const"""
        val = float(toks[0])
        return NumConst(val)

    @staticmethod
    def create_bool_const(toks: pp.ParseResults):
        """Create AST part for bool const"""
        val = str(toks[0]).lower()
        if val in ["false", "no"]:
            return BoolConst(False)
        if val in ["true", "yes"]:
            return BoolConst(True)
        raise AssertionError("One of true, yes, false, no")

    @staticmethod
    def create_str_const(toks: pp.ParseResults):
        """Create AST part for string const"""
        val = toks["body"]
        return StrConst(val)

    @staticmethod
    def create_uminus(toks: pp.ParseResults) -> ExpressionNode:
        """Create AST part for unary minus"""
        count = EvalGrammar.count_minuses(toks)
        if count % 2 == 0:
            return toks[-1]
        return OperationUnaryMinus(toks[-1])

    @staticmethod
    def count_minuses(toks):
        """Count amount of unary minuses and pluses sequenced"""
        res = 0
        for tok in toks:
            if tok == "-":
                res += 1
            elif tok == "+":
                continue
            else:
                break
        return res

    def parse(self, src: str) -> EvalAst:
        """
        Create AST for expression

        :param src: expression string
        :return: AST object create from parse tree
        """
        if src is None:
            raise EvalParsingError("no expression")
        node = self.bnf.parseString(src, parseAll=True)
        if node is None:
            raise AssertionError("nothing parsed")
        if len(node) != 1:
            raise AssertionError("must be exactly one result")
        return EvalAst(node[0])
