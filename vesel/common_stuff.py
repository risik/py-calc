"""
This module contains base exception class for 'Vesel'.
"""


class EvalError(RuntimeError):
    """Base class for 'Vesel' exceptions"""
