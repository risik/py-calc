# what is VESEL

`VESEL` is an acronym to 'VEry Simple Expression Language'

# Project's goals

* syntax analyzer example
* pyparsing library example
* AST example
* example of simple AST optimization

